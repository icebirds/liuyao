package;

import icebirds.ui.dom.DomElementProps;
import icebirds.ui.dom.Element;
import icebirds.ui.dom.RGBAColor;
import js.Browser;
import js.solarlunar.SolarLunar;

/**
 * ...
 * @author Icebirds
 */
class Main {
	static function main() {
		new Main();
	}

	function new():Void {
		var a:DomElementProps = {
			text: '测试方块',
			tag: 'p',
			type: BLOCK,
			x: 50,
			y: 30,
			w: 200,
			h: 500,
			posType: ABSOLUTE,
			bgColor: new RGBAColor(0, 0, 0, .5),
			fontColor: new RGBAColor(255, 255, 255),
			paddingTop: 20,
			paddingRight: 20,
			paddingBottom: 20,
			paddingLeft: 20,
			fontSize: 16,
			borderWidth: 0,
			borderType: 'none',
			borderRadius: 15,
			borderColor: new RGBAColor(),
			textAlign: CENTER,
			reverseX: false,
			reverseY: false,
			lineHeight: 50,
		};
		var b:Element = new Element(a);
		var c:Element = new Element(a);
		var d:Element = new Element(a);
		var e:Element = new Element(a);
		b.cutOut = false;
		c.text = '子1';
		c.w = 100;
		c.h = 60;
		c.y = (b.h - c.h) / 2 - 40;
		c.x = (b.w - c.w) / 2;
		c.padding = 0;
		d.text = '子2';
		d.w = null;
		d.h = 60;
		d.y = (b.h - d.h) / 2 + 40;
		d.x = (b.w - d.w) / 2 + 90;
		d.padding = 0;
		e.bgColor = new RGBAColor(255, 0, 0, .5);
		b.addChild(e);
		b.addChild(c);
		c.bgColor = new RGBAColor(0, 0, 170, .5);
		b.addChild(d);
		Browser.document.body.appendChild(b.node);
		//b.removeChild(e);
	}
}
