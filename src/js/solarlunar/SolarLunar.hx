package js.solarlunar;

/**
 * 引用外部农历库
 * @author Icebirds
 */
@:native('solarlunar')
extern class SolarLunar {
	@:public static function solar2lunar(year:Int, month:Int, day:Int):Dynamic;
	@:public static function lunar2solar(year:Int, month:Int, day:Int):Dynamic;
}
