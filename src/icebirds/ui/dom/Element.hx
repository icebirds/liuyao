package icebirds.ui.dom;

import js.Browser;

import icebirds.ui.dom.DomElementProps;

/**
 * 基础元素
 * @author Icebirds
 */
class Element
{
	/// 元素节点
	public var node(default, null):js.html.Element;
	var _children:Array<Element> = [];
	var _reverseX:Bool;
	var _reverseY:Bool;
	
	/// 内文
	public var text(get, set):String;
	function get_text():String{
		return node.textContent;
	}
	function set_text(value:String):String{
		node.textContent = value;
		return node.textContent;
	}
	
	/// x坐标
	public var x(get, set):Float;
	function set_x(value:Float):Float{
		if (_reverseX){
			this.node.style.right = value + "px";
			this.node.style.left = "auto";
		}
		else{
			this.node.style.left = value + "px";
			this.node.style.right = "auto";
		}
		return value;
	}
	function get_x():Float{
		if (_reverseX){
			return Std.parseFloat(this.node.style.right);
		}
		else{
			return Std.parseFloat(this.node.style.left);
		}
	}
	
	/// y坐标
	public var y(get, set):Float;
	function set_y(value:Float):Float{
		if (_reverseY){
			this.node.style.bottom = value + "px";
			this.node.style.top = "auto";
		}
		else{
			this.node.style.top = value + "px";
			this.node.style.bottom = "auto";
		}
		return value;
	}
	function get_y():Float{
		if (_reverseY){
			return Std.parseFloat(this.node.style.bottom);
		}
		else{
			return Std.parseFloat(this.node.style.top);
		}
	}
	
	/// 宽度
	public var w(get, set):Float;
	function set_w (value:Float):Float{
		this.node.style.width = value + "px";
		return value;
	}
	function get_w():Float{
		return Std.parseFloat(this.node.style.width);
	}
	
	/// 高度
	public var h(get, set):Float;
	function set_h (value:Float):Float{
		this.node.style.height = value + "px";
		return value;
	}
	function get_h():Float{
		return Std.parseFloat(this.node.style.height);
	}
	
	/// 内距
	public var padding(null, set):Float;
	function set_padding(value:Float):Float{
		this.paddingTop = value;
		this.paddingRight = value;
		this.paddingLeft = value;
		this.paddingBottom = value;
		return value;
	}
	
	/// 顶部内距
	public var paddingTop(get, set):Float;
	function set_paddingTop (value:Float):Float{
		this.node.style.paddingTop = value + "px";
		return value;
	}
	function get_paddingTop():Float{
		return Std.parseFloat(this.node.style.paddingTop);
	}
	
	/// 底部内距
	public var paddingBottom(get, set):Float;
	function set_paddingBottom (value:Float):Float{
		this.node.style.paddingBottom = value + "px";
		return value;
	}
	function get_paddingBottom():Float{
		return Std.parseFloat(this.node.style.paddingBottom);
	}
	
	/// 左侧内距
	public var paddingLeft(get, set):Float;
	function set_paddingLeft (value:Float):Float{
		this.node.style.paddingLeft = value + "px";
		return value;
	}
	function get_paddingLeft():Float{
		return Std.parseFloat(this.node.style.paddingLeft);
	}
	
	/// 右侧内距
	public var paddingRight(get, set):Float;
	function set_paddingRight (value:Float):Float{
		this.node.style.paddingRight = value + "px";
		return value;
	}
	function get_paddingRight():Float{
		return Std.parseFloat(this.node.style.paddingRight);
	}
	
	/// 行高
	public var lineHeight(get, set):Float;
	function set_lineHeight (value:Float):Float{
		this.node.style.lineHeight = value + "px";
		return value;
	}
	function get_lineHeight():Float{
		return Std.parseFloat(this.node.style.lineHeight);
	}
	
	/// 内文颜色
	public var fontColor(get, set):RGBAColor;
	private var _fontColor:RGBAColor;
	function get_fontColor():RGBAColor{
		return this._fontColor;
	}
	function set_fontColor(value:RGBAColor):RGBAColor{
		this._fontColor = value;
		this.node.style.color = value.cssString;
		return this._fontColor;
	}
	
	/// 背景颜色
	public var bgColor(get, set):RGBAColor;
	private var _bgColor:RGBAColor;
	function get_bgColor():RGBAColor{
		return this._bgColor;
	}
	function set_bgColor(value:RGBAColor):RGBAColor{
		this._bgColor = value;
		this.node.style.backgroundColor = value.cssString;
		return this._bgColor;
	}
	
	/// 文本对齐方向
	public var textAlign(get, set):TextAlign;
	var _textAlign:TextAlign;
	function get_textAlign():TextAlign{
		return _textAlign;
	}
	function set_textAlign(value:TextAlign):TextAlign{
		_textAlign = value;
		node.style.textAlign = value.getName();
		return _textAlign;
	}
	
	/// 剪切溢出
	public var cutOut(get, set):Bool;
	var _cutOut:Bool;
	function get_cutOut():Bool{
		return _cutOut;
	}
	function set_cutOut(value:Bool):Bool{
		_cutOut = value;
		node.style.overflow = value?"hidden":"visible";
		return _cutOut;
	}
	
	/**
	 * 构造函数
	 * @param	props 元素属性。
	 */
	public function new(props:DomElementProps) 
	{
		node = Browser.document.createElement(props.tag);
		node.textContent = props.text;
		node.style.margin = 0 + "px";
		node.style.overflow = "hidden";
		node.style.boxSizing = "border-box";
		
		node.style.display = props.type.getName();
		node.style.position = props.posType.getName();
		if (props.reverseY){
			node.style.top = 'auto';
			node.style.bottom = props.y + 'px';
		}else{
			node.style.bottom = 'auto';
			node.style.top = props.y + 'px';
		}
		if (props.reverseX){
			node.style.left = 'auto';
			node.style.right = props.x + 'px';
		}else{
			node.style.right = 'auto';
			node.style.left = props.x + 'px';
		}
		node.style.width = props.w + 'px';
		node.style.height = props.h + 'px';
		node.style.lineHeight = props.lineHeight + 'px';
		_fontColor = props.fontColor;
		node.style.color = props.fontColor.cssString;
		_bgColor = props.bgColor;
		node.style.backgroundColor = props.bgColor.cssString;
		node.style.fontSize = props.fontSize + "px";
		node.style.borderStyle = props.borderType;
		node.style.borderWidth = props.borderWidth + "px";
		node.style.borderRadius = props.borderRadius + 'px';
		node.style.borderColor = props.borderColor.cssString;
		node.style.textAlign = props.textAlign.getName();
		node.style.paddingTop = props.paddingTop + 'px';
		node.style.paddingLeft = props.paddingLeft + 'px';
		node.style.paddingRight = props.paddingRight + 'px';
		node.style.paddingBottom = props.paddingBottom + 'px';
	}
	
	/**
	 * 反转x轴
	 * @param	value 反转后的x值，缺省调用已有x值。
	 */
	public function reverseX(value:Float = null){
		if (value == null)
		{
			value = this.x;
		}
		_reverseX = !_reverseX;
		this.x = value;
	}
	
	/**
	 * 反转y轴
	 * @param	value 反转后的y值，缺省调用已有y值。
	 */
	public function reverseY(value:Float = null){
		if (value == null)
		{
			value = this.y;
		}
		_reverseY = !_reverseY;
		this.y = value;
	}
	
	/**
	 * 添加子节点
	 * @param	child 要添加的子节点
	 */
	public function addChild(child:Element){
		if (this._children.indexOf(child) == -1){
			this._children.push(child);
		}
		this.node.appendChild(child.node);
	}
	
	/**
	 * 移除子节点
	 * @param	child 要移除的子节点
	 */
	public function removeChild(child:Element = null){
		if (child == null){
			for (i in _children){
				this.node.removeChild(i.node);
			}
			_children = [];
		}
		if (this._children.indexOf(child) == -1){
			return;
		}
		node.removeChild(child.node);
		this._children.remove(child);
	}
}