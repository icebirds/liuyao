package icebirds.ui.dom;
import js.html.NamedNodeMap;

/**
 * @author Icebirds
 */
typedef DomElementProps =
{
	var text:String;
	var tag: String;
	var type:DomElementType;
	var x:Float;
	var y:Float;
	var w:Float;
	var h: Float;
	var posType:DomElementPosType;
	var bgColor: RGBAColor;
	var fontColor: RGBAColor;
	var fontSize: Float;
	var borderWidth: Float;
	var borderType: String;
	var borderColor: RGBAColor;
	var borderRadius: Float;
	var paddingTop: Float;
	var paddingBottom: Float;
	var paddingLeft: Float;
	var paddingRight: Float;
	var textAlign: TextAlign;
	var lineHeight: Float;
	var reverseX: Bool;
	var reverseY: Bool;
}

enum DomElementType{
	BLOCK;
	INLINE;
	INLINE_BLOCK;
}

enum DomElementPosType{
	ABSOLUTE;
	FIXED;
	STATIC;
}

enum TextAlign{
	LEFT;
	CENTER;
	RIGHT;
}