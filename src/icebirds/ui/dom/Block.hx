package icebirds.ui.dom;

/**
 * ...
 * @author Icebirds
 */
class Block extends Element 
{

	public function new(x:) 
	{
		var props:DomElementProps = {
			text: '',
			tag: 'div',
			type: BLOCK,
			x: null,
			y: null,
			w: null,
			h: null,
			posType: ABSOLUTE,
			bgColor: new RGBAColor(0, 0, 0, .5),
			fontColor: new RGBAColor(255,255,255),
			paddingTop: 0,
			paddingRight: 0,
			paddingBottom: 0,
			paddingLeft: 0,
			fontSize: 14,
			borderWidth: 1,
			borderType: solid,
			borderRadius: 5,
			borderColor: new RGBAColor(255,255,255),
			textAlign: CENTER,
			reverseX: false,
			reverseY: false,
			lineHeight: null,
		};
		super(props);
		
	}
	
}