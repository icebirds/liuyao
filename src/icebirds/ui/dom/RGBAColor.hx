package icebirds.ui.dom;

/**
 * ...
 * @author Icebirds
 */
class RGBAColor 
{
	var r: Int;
	var g: Int;
	var b: Int;
	var a: Float;
	public var cssString(get, null):String;
	public function new(r:Int = 0, g:Int = 0, b:Int = 0, a:Float = 1) 
	{
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}
	
	function get_cssString():String{
		return 'rgba($r,$g,$b,$a)';
	}
	
}